# Le concept 

Jeu vidéo simple, 2D, vue de côté, la caméra suit une balle affectée par la gravité.
Le joueur trace des courbes à l'aide de son curseur/doigt afin de créer un sol sur lequel une balle peut rouler.

Le but du jeu est de survivre le plus longtemps, la seule condition de de défaite étant de tomber dans la lave.

Le monde dans lequel évolue la balle est composé d'obstacles comme des surfaces rebondissantes/gluantes et de bonus/malus tels que la modification de la masse de la balle, longueur du tracé de la courbe, variation du champ de vision... 

Gameplay nerveux, durée d'une run entre 2 et 5 minutes.

# Tests utilisateurs

Je compte partager mon avancée à chaque nouveau lot de features qui méritent un feedback de votre part.

Pour cela, vous avez juste à tester le jeu en cliquant sur le lien de la démo. 

De plus, je vous invite à remplir le Google Forms le plus récent afin de partager vos impressions, conseils ou bugs que vous rencontrez.

---

### Demo : https://sandwitcher.gitlab.io/the-floor-is-lava/

---

### 28/04/2021 : Tracé de la courbe - Preview de la direction de la balle - Suivi de la caméra - Outils de test utilisateur - Participation :  https://forms.gle/cfYxtBKFWbrSoYVMA

Essentiellement il faut tester la mécanique coeur, il n'y a que le tracé de courbe implémenté, avec quelques boutons pour reset ou donner de la force à la balle. Juste à faire des tracés et voir si c'est feels good.

---
