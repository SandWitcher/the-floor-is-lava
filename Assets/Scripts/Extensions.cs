﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Extensions
{
    public static bool ApproximatelyOr(this Vector2 original, Vector2 compared)
    {
        return Mathf.Approximately(original.x, compared.x) || Mathf.Approximately(original.y, compared.y);
    }
    
    public static bool ApproximatelyOr(this Vector2 original, Vector2 compared, float approx)
    {
        return Mathf.Abs(original.x - compared.x) < approx || Mathf.Abs(original.y -  compared.y) < approx;
    }

    public static bool ApproximatelyAnd(this Vector2 original, Vector2 compared)
    {
        return Mathf.Approximately(original.x, compared.x) && Mathf.Approximately(original.y, compared.y);
    }

    public static bool ApproximatelyAnd(this Vector2 original, Vector2 compared, float approx)
    {
        return Mathf.Abs(original.x - compared.x) < approx && Mathf.Abs(original.y -  compared.y) < approx;
    }

    public static Vector2 Direction(this Vector2 original, Vector2 destination)
    {
        return destination - original;
    }
    
    public static Vector3[] ToVector3Array(this List<Vector2> original)
    {
        return original.Select(v => (Vector3) v).ToArray();
    }
    
    public static List<Vector2> ToVector2List(this IEnumerable<Vector3> original)
    {
        return original.Select(v => (Vector2) v).ToList();
    }
    
    public static List<T> Clone<T>(this List<T> source) {
        return source.GetRange(0, source.Count);
    }

    public static bool IsOfType<T>(this object obj) => obj is T;
    public static bool IsOfType<T1, T2>(this object obj) => obj is T1 || obj is T2;
    public static bool IsOfType<T1, T2, T3>(this object obj) => obj is T1 || obj is T2 || obj is T3;
    public static bool IsOfType<T1, T2, T3, T4>(this object obj) => obj is T1 || obj is T2 || obj is T3 || obj is T4;
}