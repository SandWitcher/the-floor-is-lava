﻿using Ball.ScriptableObjects;
using UnityEngine;

namespace Ball
{
    public class BallTarget
    {
        private readonly BallData _ballData;
        private readonly BallSettings _ballSettings;
        private readonly Transform _targetTransform;

        private readonly Vector2 _velocityFactor = new Vector2(0.6f, 0.2f);
        private readonly float _smoothTime = 0.5f;
        private readonly float _targetMaxDistance = 10f;
        
        private Vector2 _targetVelocity;
        private Vector2 _targetPosition;

        public BallTarget(BallData ballData, BallSettings ballSettings, Transform targetTransform)
        {
            _ballData = ballData;
            _ballSettings = ballSettings;
            _targetTransform = targetTransform;
            
            _targetTransform.position = Vector3.zero;
            _targetVelocity = Vector2.zero;
        }

        public void Tick()
        {
            _targetPosition.x = _ballData.velocity.x * _velocityFactor.x;
            _targetPosition.y = _ballData.velocity.y * _velocityFactor.y;

            if (_targetPosition.magnitude > _targetMaxDistance)
                _targetPosition = _targetPosition.normalized * _targetMaxDistance;
            
            _targetTransform.localPosition = Vector2.SmoothDamp(
                _targetTransform.localPosition, 
                _targetPosition, 
                ref _targetVelocity,
                _smoothTime);
        }
    }
}