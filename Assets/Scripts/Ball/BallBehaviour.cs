﻿using Ball.ScriptableObjects;
using UnityEngine;

namespace Ball
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class BallBehaviour : MonoBehaviour
    {
        [SerializeField] 
        private BallData ballData;

        [SerializeField] 
        private BallSettings ballSettings;
        
        private Ball _ball;
        private BallTarget _ballTarget;
        private BallEffect _ballEffect;
        
        private void Awake()
        {
            _ball = new Ball(ballData, ballSettings,GetComponent<Rigidbody2D>());
            _ballTarget = new BallTarget(ballData, ballSettings, transform.GetChild(0));
            _ballEffect = new BallEffect(ballData, ballSettings, GetComponent<SpriteRenderer>());
        }

        private void Update()
        {
            _ball.Tick(); // call first to set BallData values
            _ballTarget.Tick();
            _ballEffect.Tick();
        }

        private void OnDestroy()
        {
            ballData.Reset();
        }
    }
}
