﻿using System;
using UnityEngine;

namespace Ball
{
	public class BallController : MonoBehaviour
	{
		private Rigidbody2D _rb;

		private void Awake() => _rb = GetComponent<Rigidbody2D>();

		public void AddForceX(int x)
		{
			_rb.AddForce(new Vector2(x, 0));
		}

		public void AddForceY(int y)
		{
			_rb.AddForce(new Vector2(0, y));
		}

		public void ResetBall()
		{
			_rb.velocity = Vector2.zero;
			_rb.position = Vector2.zero;
		}
	}
}