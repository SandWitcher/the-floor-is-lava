﻿using UnityEngine;

namespace Ball.ScriptableObjects
{
    [CreateAssetMenu(fileName = "BallSettings", menuName = "Game/Ball/Settings", order = 0)]
    public class BallSettings : ScriptableObject
    {
        public Vector2 maxVelocity;

        public Vector2 minVelocity;

        public float flatVelocityIncrease;

    
        public float minGravity;

        public float maxGravity;

        public float yVelocityGravityFactor;
    
    
        public Color[] colors;
    }
}