﻿using System;
using UnityEngine;

namespace Ball.ScriptableObjects
{
        [CreateAssetMenu(fileName = "BallData", menuName = "Game/Ball/Data", order = 0)]
        public class BallData : ScriptableObject
        {
                public Vector2 position;

                public Vector2 velocity;

                public float velocityMagnitude;
                
                public void Reset()
                {
                        position = Vector2.zero;
                        velocity = Vector2.zero;
                        velocityMagnitude = 0;
                }
        }
}