﻿using Ball.ScriptableObjects;
using UnityEngine;

namespace Ball
{
    public class BallEffect
    {
        private readonly BallData _ballData;
        private readonly BallSettings _ballSettings;
        private readonly SpriteRenderer _spriteRenderer;

        private readonly float _colorTreshold;
        
        public BallEffect(BallData ballData, BallSettings ballSettings, SpriteRenderer spriteRenderer)
        {
            _ballData = ballData;
            _ballSettings = ballSettings;
            _spriteRenderer = spriteRenderer;

            _colorTreshold = (_ballSettings.maxVelocity.magnitude - _ballSettings.minVelocity.magnitude) / _ballSettings.colors.Length;
        }

        public void Tick()
        {
            int colorIndex = Mathf.FloorToInt(_ballData.velocityMagnitude / _colorTreshold);
            if (colorIndex >= _ballSettings.colors.Length - 1) return;

            float colorValue = _ballData.velocityMagnitude % _colorTreshold;
            
            _spriteRenderer.color = Color.Lerp(
                _ballSettings.colors[colorIndex],
                _ballSettings.colors[colorIndex + 1],
                colorValue / _colorTreshold);
        }
    }
}