﻿using Ball.ScriptableObjects;
using UnityEngine;

namespace Ball
{
    public class Ball
    {
        private BallData _ballData;
        private readonly BallSettings _ballSettings;
        private Rigidbody2D _rigidbody2D;

        public Ball(BallData ballData, BallSettings ballSettings, Rigidbody2D rigidbody2D)
        {
            _ballData = ballData;
            _ballSettings = ballSettings;
            _rigidbody2D = rigidbody2D;
        }

        public void Tick()
        {
            _ballData.position = _rigidbody2D.position;
            _ballData.velocity = _rigidbody2D.velocity;
            _ballData.velocityMagnitude = _ballData.velocity.magnitude;
        }
    }
}