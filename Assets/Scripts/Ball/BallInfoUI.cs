﻿using Ball.ScriptableObjects;
using TMPro;
using UnityEngine;

namespace Ball
{
	public class BallInfoUI : MonoBehaviour
	{
		[SerializeField] 
		private BallData data;


		[SerializeField] private TextMeshProUGUI velocityText;
		[SerializeField] private TextMeshProUGUI magnitudeText;
		[SerializeField] private TextMeshProUGUI gravityScaleText;

		private void Awake()
		{
			gravityScaleText.SetText("1.0");
		}

		private void Update()
		{
			velocityText.SetText(data.velocity.ToString()); 
			magnitudeText.SetText(data.velocityMagnitude.ToString("#.#")); 
		}
	}
}