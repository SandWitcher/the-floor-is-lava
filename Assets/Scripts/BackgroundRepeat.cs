﻿using System;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class BackgroundRepeat : MonoBehaviour
{

    [SerializeField]
    [Range(0.01f, 1f)]
    private float factor;
    [SerializeField]
    private Rigidbody2D player;
    
    private MeshRenderer _meshRenderer;
    private static readonly int MainTex = Shader.PropertyToID("_MainTex");

    private Vector2 _offset;
    private void Awake()
    {
        _offset = Vector2.zero;
        _meshRenderer = GetComponent<MeshRenderer>();
        _meshRenderer.sharedMaterial.SetTextureOffset(MainTex, _offset);
    }

    private void Update()
    {
        _offset += player.velocity * (Time.deltaTime * factor);
        _meshRenderer.sharedMaterial.SetTextureOffset(MainTex, _offset);
    }

    private void OnDestroy()
    {
        _meshRenderer.sharedMaterial.SetTextureOffset(MainTex, Vector2.zero);
    }
}