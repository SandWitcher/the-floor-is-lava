﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DrawLine2D.Debug
{
    [RequireComponent(typeof(VerticalLayoutGroup))]
    public class DrawLine2DDebugUI : MonoBehaviour
    {
        [SerializeField] private GameObject debugTextPrefab;

        [SerializeField] private bool enableDebug;

        private readonly List<Type> _acceptedTypes = new List<Type>()
        {
            typeof(bool),
            typeof(string),
            typeof(Vector2),
            typeof(Vector3),
            typeof(int),
            typeof(float)
        };
        
        private void Start() => SetPropertyRows();

        private void OnValidate()
        {
            #if UNITY_EDITOR
            if (EditorApplication.isPlaying) SetPropertyRows();
            #endif
        }
    
        private void SetPropertyRows()
        {
            if (enableDebug && transform.childCount <= 0) SetDrawLine2DInput(FindObjectOfType<DrawLine2DBehaviour>().DrawLine2DInput);
            gameObject.SetActive(enableDebug);
        }

        private void SetDrawLine2DInput(IDrawLine2DInput input)
        {
            if (input == null) return;
        
            foreach (var property in input.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                if (!_acceptedTypes.Contains(property.PropertyType)) continue;
                var propertyText = Instantiate(debugTextPrefab, transform);
                propertyText.GetComponent<DrawLine2DDebugUIProperty>().SetProperty(input, property);
            }
        }
    }
}
