﻿using System.Reflection;
using TMPro;
using UnityEngine;

namespace DrawLine2D.Debug
{
    public class DrawLine2DDebugUIProperty : MonoBehaviour
    {
        public TextMeshProUGUI nameText;
        public TextMeshProUGUI valueText;

        private PropertyInfo _property;
        private IDrawLine2DInput _drawLine2DInput;

        private bool _isBool;
        private void Update()
        {
            if (_property == null) return;

            if (_isBool)
            {
                var value = _property.GetValue(_drawLine2DInput) as bool?;
                valueText.color = value!.Value ? Color.green : Color.red;
                valueText.SetText(value.ToString());
            }
            else
            {
                valueText.SetText(_property.GetValue(_drawLine2DInput).ToString());
            }
        }

        public void SetProperty(IDrawLine2DInput input, PropertyInfo property)
        {
            _drawLine2DInput = input;
            _isBool = property.PropertyType == typeof(bool);
            _property = property;
            nameText.SetText(property.Name);
            name = property.Name;
        }
    }
}