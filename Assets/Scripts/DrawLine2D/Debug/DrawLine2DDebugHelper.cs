﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DrawLine2D.Debug
{
	[RequireComponent(typeof(SpriteRenderer))]
	public class DrawLine2DDebugHelper : MonoBehaviour
	{
		private SpriteRenderer _spriteRenderer;
		private Coroutine _coroutine;
		
		private void Awake() => _spriteRenderer = GetComponent<SpriteRenderer>();

		public void ShowPoint(Vector2 point, float duration)
		{
			if (_coroutine != null) StopCoroutine(_coroutine);
			_coroutine = StartCoroutine(ShowPointCoroutine(point, duration));
		}

		private IEnumerator ShowPointCoroutine(Vector2 point, float duration)
		{
			transform.position = point;
			_spriteRenderer.color = Color.Lerp(Color.magenta, Color.yellow, Random.value);
			_spriteRenderer.enabled = true;
			yield return new WaitForSeconds(duration);
			// ReSharper disable once Unity.InefficientPropertyAccess
			_spriteRenderer.enabled = false;
		}
	}
}