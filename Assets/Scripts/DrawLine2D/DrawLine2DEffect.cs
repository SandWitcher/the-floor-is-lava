﻿using DrawLine2D.ScriptableObjects;
using UnityEngine;

namespace DrawLine2D
{
	public class DrawLine2DEffect : ADrawLine2DBase
	{
		private readonly RectTransform _rectTransform;
		private readonly ParticleSystem[] _particleSystems;
		
		public DrawLine2DEffect(IDrawLine2DInput input, DrawLine2DSettings settings, DrawLine2DData data, GameObject particleSystemParent) : base(input, settings, data)
		{
			_rectTransform = particleSystemParent.GetComponent<RectTransform>();
			_particleSystems = particleSystemParent.GetComponentsInChildren<ParticleSystem>();
		}

		public override void Tick()
		{
			if (input.Reset)
			{
				_rectTransform.position = input.WorldPoint;
				_particleSystems[0].Play(true);
			}

			if (input.CancelPreview || input.HasFinishedPreview)
			{
				_particleSystems[0].Stop(true);
				return;
			}

			if (input.IsPreviewing)
			{
				_rectTransform.position = Vector2.MoveTowards(_rectTransform.position, input.WorldPoint, 1f);
			}
		}
	}
}