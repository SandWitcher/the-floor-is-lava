﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DrawLine2D.Tests.EditMode
{
	[CreateAssetMenu(fileName = "DrawLine2DEditModeTest", menuName = "Tests/DrawLine2D/EditMode", order = 0)]
	public class DrawLine2DEditModeTestSO : ADrawLine2DTestSO
	{
		[Header("Loopings")] 
		
		public List<Points> loopings = new List<Points>();

		public List<Points> notLoopings = new List<Points>();
		
		public void FeedLooping(List<Vector2> points) => loopings.Add(new Points(points));
		
		public void FeedNotLooping(List<Vector2> points) => notLoopings.Add(new Points(points));

		public void ClearLoopings() => loopings.Clear();

		public void ClearNotLoopings() => notLoopings.Clear();
	}
}