using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Sirenix.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DrawLine2D.Tests.EditMode
{
    [RequireComponent(typeof(VerticalLayoutGroup))]
    public class DrawLine2DEditModeTestFeeder : MonoBehaviour
    {
        public DrawLine2DEditModeTestSO drawLine2DEditModeTestSO;

        private DrawLine2DBehaviour _drawLine2DBehaviour;

        public GameObject buttonPrefab;
        
        private void Awake()
        {
#if !UNITY_EDITOR
            return;
#endif
            
            _drawLine2DBehaviour = FindObjectOfType<DrawLine2DBehaviour>().GetComponent<DrawLine2DBehaviour>();

            if (_drawLine2DBehaviour == null)
            {
                throw new UnityException("DrawLine2DBehaviour not found in scene");
            }
            
            var methods =  typeof(DrawLine2DEditModeTestSO).GetMethods(
                bindingAttr: BindingFlags.Instance | 
                             BindingFlags.Public | 
                             BindingFlags.DeclaredOnly);
            
            GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,
                (methods.Length * buttonPrefab.GetComponent<RectTransform>().rect.height));
            
            foreach (var method in methods)
            {
                var button = Instantiate(buttonPrefab, transform);
                button.name = method.Name;
                button.GetComponentInChildren<TextMeshProUGUI>().SetText(method.Name);

                if (method.HasParamaters(new List<Type> {typeof(List<Vector2>)}))
                {
                    button.GetComponent<Button>().onClick.AddListener(() => 
                        method.Invoke(drawLine2DEditModeTestSO, new object[] {_drawLine2DBehaviour.GetComponent<EdgeCollider2D>().points.ToList()}));
                }
                else
                {
                    button.GetComponent<Button>().onClick.AddListener(() => 
                        method.Invoke(drawLine2DEditModeTestSO, null));
                }
            }
        }
    }
}
