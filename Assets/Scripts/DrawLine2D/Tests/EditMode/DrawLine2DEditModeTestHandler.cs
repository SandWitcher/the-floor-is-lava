﻿using System.Collections.Generic;
using UnityEngine;

namespace DrawLine2D.Tests.EditMode
{
	[RequireComponent(typeof(LineRenderer))]
	public class DrawLine2DEditModeTestHandler : MonoBehaviour
	{
		public DrawLine2DEditModeTestSO drawLine2DEditModeTestSO;

		private LineRenderer _lineRenderer;

		private int _loopingIndex, _notLoopingIndex;
		
		private void Awake() => _lineRenderer = GetComponent<LineRenderer>();

		private void Draw(List<Vector2> points)
		{
			_lineRenderer.positionCount = points.Count;
			_lineRenderer.SetPositions(points.ToVector3Array());
		}

		public void ShowNextLooping()
		{
			if (drawLine2DEditModeTestSO.loopings.Count <= 0) return;
			_loopingIndex = _loopingIndex < drawLine2DEditModeTestSO.loopings.Count - 1 ? _loopingIndex + 1 : 0;
			UnityEngine.Debug.Log($"loopings[{_loopingIndex}]");
			Draw(drawLine2DEditModeTestSO.loopings[_loopingIndex].points);
		}
		
		public void ShowNextNotLooping()
		{
			if (drawLine2DEditModeTestSO.notLoopings.Count <= 0) return;
			_notLoopingIndex = _notLoopingIndex < drawLine2DEditModeTestSO.notLoopings.Count - 1 ? _notLoopingIndex + 1 : 0;
			UnityEngine.Debug.Log($"notLoopings[{_notLoopingIndex}]");
			Draw(drawLine2DEditModeTestSO.notLoopings[_notLoopingIndex].points);
		}
	}
}