﻿using System.Linq;
using NSubstitute;
using UnityEngine;

// ReSharper disable ClassWithVirtualMembersNeverInherited.Global
// ReSharper disable UnassignedGetOnlyAutoProperty

namespace DrawLine2D.Tests
{
	// ReSharper disable once ClassNeverInstantiated.Global
	public class MockDrawLine2DInput : IDrawLine2DInput
	{
		public Camera DrawCamera { get; }
		
		
		public virtual bool IsOverUI { get; }
		public virtual bool Reset { get; }
		public virtual bool IsPreviewing { get; }
		public virtual bool CancelPreview { get; }
		public virtual bool HasFinishedPreview { get; }
		public virtual bool HasCanceledPreview { get ; }
		
		
		public Vector2 PreviousMouseInput { get; private set; }
		public Vector2 MouseInput { get; private set; }
		public Vector2 PreviousWorldPoint { get; private set; }
		public Vector2 WorldPoint { get; private set; }
		public Vector2 PreviousViewportPoint { get; private set; }
		public Vector2 ViewportPoint { get; private set; }

		//private ADrawLine2DTestSO _testSO;

		private readonly int _screenWidth;
		private readonly int _screenHeight;
		
		public MockDrawLine2DInput(Camera drawCamera)
		{
			DrawCamera = drawCamera;
			_screenWidth = Screen.width;
			_screenHeight = Screen.height;
		}

		// public MockDrawLine2DInput(Camera drawCamera, ADrawLine2DTestSO testSO) : this (drawCamera)
		// {
		// 	_testSO = testSO;
		// }
		
		public void ReadInput()
		{
			PreviousMouseInput = MouseInput;
			MouseInput = new Vector2(Random.Range(0, _screenWidth), Random.Range(0, _screenHeight));	
			
			PreviousViewportPoint = ViewportPoint;
			ViewportPoint = DrawCamera.ScreenToViewportPoint(MouseInput);
			
			PreviousWorldPoint = WorldPoint;
			WorldPoint = DrawCamera.ScreenToWorldPoint(MouseInput);
		}

		public void LateTick()
		{
			if (Reset) HasCanceledPreview.Returns(false);
			if (IsPreviewing && CancelPreview) HasCanceledPreview.Returns(true);
		}

		public Vector2[] ViewportToWorldPoints(Vector2[] points)
		{
			return points.Select(p => (Vector2) DrawCamera.ViewportToWorldPoint(p)).ToArray();
		}

		public void ResetMockedProperties()
		{
			IsOverUI.Returns(false);
			Reset.Returns(false);
			IsPreviewing.Returns(false);
			CancelPreview.Returns(false);
			HasFinishedPreview.Returns(false);
			HasCanceledPreview.Returns(false);
		}
	}
}