﻿using System;
using System.Collections.Generic;
using DrawLine2D.ScriptableObjects;
using UnityEngine;

namespace DrawLine2D.Tests
{
	public class ADrawLine2DTestSO : ScriptableObject
	{
		[Serializable]
		public class Points
		{
			public List<Vector2> points;
			public Points(List<Vector2> points) => this.points = points; 
		}

		[Serializable]
		public class DataSnapShot : ICloneable
		{
			public List<Vector2> worldPoints;
			public List<Vector2> previewPoints;
			public List<Vector2> worldBezierPoints;
			public List<Vector2> bezierPoints;

			public DataSnapShot(DrawLine2DData data)
			{
				if (data == null) return;
				
				worldPoints = data.worldPoints.Clone();
				previewPoints = data.previewPoints.Clone();
				worldBezierPoints = data.worldBezierPoints.Clone();
				bezierPoints = data.bezierPoints.Clone();
			}

			public object Clone()
			{
				return (DataSnapShot) MemberwiseClone();
			}
		}
	}
}