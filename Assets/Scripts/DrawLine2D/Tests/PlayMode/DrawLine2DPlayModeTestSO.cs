﻿using UnityEngine;

namespace DrawLine2D.Tests.PlayMode
{
	[CreateAssetMenu(fileName = "DrawLine2DPlayModeTest", menuName = "Tests/DrawLine2D/PlayMode", order = 0)]
	public class DrawLine2DPlayModeTestSO : ADrawLine2DTestSO
	{
		
	}
}