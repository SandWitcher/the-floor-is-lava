﻿using System.Collections.Generic;
using UnityEngine;

namespace DrawLine2D
{
	public interface IDrawLine2D
	{
		public void Draw(List<Vector2> points);

		public void ResetPoints(ref List<Vector2> points);
	}
}