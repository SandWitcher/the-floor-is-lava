﻿using UnityEngine;

namespace DrawLine2D.ScriptableObjects
{
    [CreateAssetMenu(fileName = "DrawLine2DSettings", menuName = "Game/DrawLine2D/Settings", order = 0)]
    public class DrawLine2DSettings : ScriptableObject
    {
        [Header("Base")]
        [Range(0f, 1f)]
        public float smoothDistanceTrigger;

        
        [Header("Looping")] 
        public bool enableLooping;

        [Range(0f, 1f)] 
        public float initialIndexGapFactor;

        [Range(0f, 1f)] 
        public float loopingDistanceDetection;

        [Header("Preview")]
        
        [Range(0f, 10f)]
        public float previewMinDistanceBetweenTwoPoints;

        [Header("World")] 
        
        [Range(0f, 1f)]
        public float worldSegmentSize;
    }
}
