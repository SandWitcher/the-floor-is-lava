﻿using System.Collections.Generic;
using UnityEngine;

namespace DrawLine2D.ScriptableObjects
{
    [CreateAssetMenu(fileName = "DrawLine2DData", menuName = "Game/DrawLine2D/Data", order = 0)]
    public class DrawLine2DData : ScriptableObject
    {
        public List<Vector2> worldPoints;

        public List<Vector2> previewPoints;

        public List<Vector2> worldBezierPoints;
        
        public List<Vector2> bezierPoints;
        
        private void OnDisable()
        {
            Reset();
        }

        public void Reset()
        {
            worldPoints.Clear();
            previewPoints.Clear();
            
            for (int i = 0; i < bezierPoints.Count; i++) bezierPoints[i] = Vector2.zero;
            for (int i = 0; i < worldBezierPoints.Count; i++) worldBezierPoints[i] = Vector2.zero;
        }
    }
}