﻿using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DrawLine2D
{
    public class StandaloneInput : IDrawLine2DInput
    {
        private readonly EventSystem _eventSystem;

        #region References

        public Camera DrawCamera { get; private set; }

        #endregion
        
        
        #region Input triggers

        public bool IsOverUI { get; private set; }
        public bool Reset { get; private set; }
        public bool IsPreviewing { get; private set; }
        public bool CancelPreview { get; private set; }
        public bool HasFinishedPreview { get; private set; }
        public bool HasCanceledPreview { get; private set; }

        #endregion
        

        #region Mouse Inputs

        public Vector2 PreviousMouseInput { get; private set;  }
        public Vector2 MouseInput { get; private set; }

        #endregion
        

        #region World Positions

        public Vector2 PreviousWorldPoint { get; private set; }
        public Vector2 WorldPoint { get; private set; }
        
        #endregion


        #region Viewport Positions

        public Vector2 PreviousViewportPoint { get; private set; }
        public Vector2 ViewportPoint { get; private set; }
        
        #endregion
        
        public StandaloneInput(Camera drawCamera)
        {
            _eventSystem = EventSystem.current;
            DrawCamera = drawCamera;
        }

        public void ReadInput()
        {
            IsOverUI = _eventSystem.IsPointerOverGameObject();
            
            Reset = Input.GetKeyDown(KeyCode.Mouse0);
            CancelPreview = Input.GetKeyDown(KeyCode.Mouse1);
            
            IsPreviewing = Input.GetKey(KeyCode.Mouse0);
            
            HasFinishedPreview = Input.GetKeyUp(KeyCode.Mouse0);

            PreviousMouseInput = MouseInput;
            MouseInput = Input.mousePosition;

            PreviousViewportPoint = ViewportPoint;
            ViewportPoint = DrawCamera.ScreenToViewportPoint(MouseInput);

            PreviousWorldPoint = WorldPoint;
            WorldPoint = DrawCamera.ScreenToWorldPoint(MouseInput);
        }

        public void LateTick()
        {
            if (Reset) HasCanceledPreview = false;
            if (IsPreviewing && CancelPreview) HasCanceledPreview = true;
        }
        
        public Vector2[] ViewportToWorldPoints(Vector2[] points)
        {
            return points.Select(p => (Vector2) DrawCamera.ViewportToWorldPoint(p)).ToArray();
        }
    }
}
