﻿using UnityEngine;

namespace DrawLine2D
{
    public class DrawLine2DCleaner
    {
        private readonly Transform _ballTransform;
        private readonly Rigidbody2D _ballRigidbody2D;
        private readonly EdgeCollider2D _edgeCollider2D;
    
        public DrawLine2DCleaner(Transform ballTransform, EdgeCollider2D edgeCollider2D)
        {
            _ballTransform = ballTransform;
            _ballRigidbody2D = ballTransform.GetComponent<Rigidbody2D>();
            _edgeCollider2D = edgeCollider2D;
        }

        #region Collision handlers

        public void OnCollisionEnter2D(Collision2D other)
        {

        }

        public void OnCollisionStay2D(Collision2D other)
        {
     
        }

        public void OnCollisionExit2D(Collision2D other)
        {
      
        }
	
        #endregion

        #region Trigger handlers

        public void OnTriggerEnter2D(Collider2D other)
        {
        
        }

        public void OnTriggerStay2D(Collider2D other)
        {
        
        }

        public void OnTriggerExit2D(Collider2D other)
        {
        
        }

        #endregion
    }
}