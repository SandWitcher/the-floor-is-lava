﻿using System.Collections.Generic;
using System.Linq;
using DrawLine2D.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI.Extensions;

namespace DrawLine2D
{
	public class DrawLine2DBezier : ADrawLine2DBase, IDrawLine2D
	{
		private readonly UILineRenderer _uiLineRenderer;
		
		public DrawLine2DBezier(IDrawLine2DInput input, DrawLine2DSettings settings, DrawLine2DData data, UILineRenderer uiLineRenderer) : base(input, settings, data)
		{
			_uiLineRenderer = uiLineRenderer;
			data.bezierPoints = Enumerable.Repeat(Vector2.zero, 4).ToList();
		}

		public override void Tick()
		{
			if (input.Reset)
			{
				data.bezierPoints[0] = input.ViewportPoint;
				return;
			}

			if (input.HasCanceledPreview) return;
			
			if (input.IsPreviewing)
			{
				if (input.CancelPreview)
				{
					ResetPoints(ref data.bezierPoints);
					Draw();
					return;
				}
				
				UpdateControlPoints();
				Draw(data.bezierPoints);
			}
			
			if (input.HasFinishedPreview)
			{
				UpdateControlPoints();
				Draw();
			}
		}

		public void LateTick()
		{
			if (input.HasFinishedPreview) ResetPoints(ref data.bezierPoints);
		}

		private void UpdateControlPoints()
		{
			var size = data.previewPoints.Count;

			data.bezierPoints[1] = data.previewPoints[Mathf.FloorToInt(size / 3f)];
			data.bezierPoints[2] = data.previewPoints[Mathf.FloorToInt(2 * size / 3f)];
			data.bezierPoints[3] = data.previewPoints[size - 1];
		}
		
		
		public void Draw(List<Vector2> points = null)
		{
			_uiLineRenderer.Points = points != null ? points.ToArray() : new []{Vector2.zero};
		}

		public void ResetPoints(ref List<Vector2> points)
		{
			for (int i = 0; i < points.Count; i++)
			{
				points[i] = Vector2.zero;
			}
		}
	}
}

// private float CalculateCurrentAngle(Vector2[] points)
// {
// float averageAngle = 0;
//
// for (int i = 0; i < points.Length - 1; i++)
// {
// 	averageAngle += Vector2.Angle(points[i], points[i + 1]);
// }
//
// averageAngle /= BatchSize;
//
// return averageAngle;
//
// return Vector2.Angle(points[0], points[points.Length - 1]);
//}

// private class ControlPoints
// {
// 	public readonly Vector2[] controlPoints = new Vector2[3];
//
// 	public readonly int startIndex;
//
// 	public int endIndex;
//
// 	public ControlPoints(int startIndex)
// 	{
// 		this.startIndex = startIndex;
// 	}
// }