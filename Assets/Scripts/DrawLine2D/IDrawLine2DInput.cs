﻿using UnityEngine;

namespace DrawLine2D
{
    public interface IDrawLine2DInput
    {
        #region References

        public Camera DrawCamera { get; }

        #endregion
        
        
        #region Input Triggers

        bool IsOverUI { get; }
        bool Reset { get; }
        bool IsPreviewing { get; }
        bool CancelPreview { get; }
        bool HasFinishedPreview { get; }
        bool HasCanceledPreview { get; }
        
        
        #endregion


        #region Mouse Inputs
        
        public Vector2 PreviousMouseInput { get; }
        public Vector2 MouseInput { get; }
        
        #endregion


        #region World Positions

        public Vector2 PreviousWorldPoint { get; }
        public Vector2 WorldPoint { get; }
        
        #endregion


        #region Viewport Positions

        public Vector2 PreviousViewportPoint { get; }
        public Vector2 ViewportPoint { get; }
        
        #endregion
        
        #region Methods
        
        void ReadInput();

        void LateTick();

        Vector2[] ViewportToWorldPoints(Vector2[] points);

        #endregion
    }
}