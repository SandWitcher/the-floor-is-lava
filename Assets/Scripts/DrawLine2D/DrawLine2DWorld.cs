﻿using System.Collections.Generic;
using System.Linq;
using DrawLine2D.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI.Extensions;

namespace DrawLine2D
{
    public class DrawLine2DWorld : ADrawLine2DBase, IDrawLine2D
    {
        private readonly LineRenderer _lineRenderer;
        private readonly EdgeCollider2D _edgeCollider2D;
        
        public DrawLine2DWorld(IDrawLine2DInput input, DrawLine2DSettings settings, DrawLine2DData data, LineRenderer lineRenderer, EdgeCollider2D edgeCollider2D) : base(input, settings, data)
        {
            _lineRenderer = lineRenderer;
            _edgeCollider2D = edgeCollider2D;
        }
        
        public override void Tick()
        {
            if (input.HasFinishedPreview && !input.HasCanceledPreview && data.previewPoints.Count >= 2)
            {
                if (settings.enableLooping) CheckIsLooping(data.worldPoints);
                ResetPoints(ref data.worldPoints);

                data.worldBezierPoints = input.ViewportToWorldPoints(data.bezierPoints.ToArray()).ToList();
                
                data.worldPoints = UpdateWorldPoints(data.worldBezierPoints.ToArray()).ToList();
                Draw(data.worldPoints);
            }
        }
        
        public void ResetPoints(ref List<Vector2> points)
        {
            _lineRenderer.positionCount = 0;
            _edgeCollider2D.enabled = false;
        }

        public void Draw(List<Vector2> points)
        {
            if (points.Count < 2) return;
            _edgeCollider2D.SetPoints(points);
            _edgeCollider2D.enabled = true;
            _lineRenderer.positionCount = points.Count;
            _lineRenderer.SetPositions(points.ToVector3Array());
        }

        private Vector2[] UpdateWorldPoints(Vector2[] worldBezierPoints)
        {
            Vector2[] points = new Vector2[101];
            
            int i = 0;
            
            for (float time = 0; time < 1; time += 0.01f)
            {
                points[i] = UILineRenderer.GetPoint(worldBezierPoints, time);
                i++;
            }

            return points;
        }
    }
}