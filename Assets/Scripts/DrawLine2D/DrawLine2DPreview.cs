﻿using System.Collections.Generic;
using DrawLine2D.ScriptableObjects;
using UnityEngine;

namespace DrawLine2D
{
    public class DrawLine2DPreview : ADrawLine2DBase, IDrawLine2D
    {
        public DrawLine2DPreview(IDrawLine2DInput input, DrawLine2DSettings settings, DrawLine2DData data) : base(input, settings, data)
        {
        }

        public override void Tick()
        {
            if (input.Reset)
            {
                ResetPoints(ref data.previewPoints);
                data.previewPoints.Add(input.ViewportPoint);
            }
            
            if (input.IsPreviewing)
            {
                if (input.CancelPreview)
                {
                    ResetPoints(ref data.previewPoints);
                    return;
                }

                if (input.HasCanceledPreview) return;
                
                if (!input.MouseInput.ApproximatelyOr(input.PreviousMouseInput))
                {
                    data.previewPoints.Add(input.ViewportPoint);
                }
            }

            if (input.HasFinishedPreview && !input.HasCanceledPreview)
            {
                if (!input.MouseInput.ApproximatelyOr(input.PreviousMouseInput))
                {
                    data.previewPoints.Add(input.ViewportPoint);
                }
            }
        }

        public void LateTick()
        {
            if (input.HasFinishedPreview)
            {
                ResetPoints(ref data.previewPoints);
            }
        }
        
        public void Draw(List<Vector2> points = null)
        {
            throw new UnityException("Draw not implemented");    
        }

        public void ResetPoints(ref List<Vector2> points) => points!.Clear();
        
    }
}