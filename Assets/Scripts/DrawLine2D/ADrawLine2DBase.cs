﻿using System.Collections.Generic;
using DrawLine2D.Debug;
using DrawLine2D.ScriptableObjects;
using UnityEngine;

namespace DrawLine2D
{
    public abstract class ADrawLine2DBase
    {
        protected readonly IDrawLine2DInput input;
        protected readonly DrawLine2DSettings settings;
        protected readonly DrawLine2DData data;

        public DrawLine2DDebugHelper DrawLine2DDebugHelper { get; set; }
        
        protected ADrawLine2DBase(IDrawLine2DInput input, DrawLine2DSettings settings, DrawLine2DData data)
        {
            this.input = input;
            this.settings = settings;
            this.data = data;
        }

        public abstract void Tick();

        public virtual bool CheckIsLooping(List<Vector2> points)
        {
            int size = points.Count;

            int initialStep = Mathf.FloorToInt(size * settings.initialIndexGapFactor);
            
            for (int i = 0; i < points.Count - initialStep; i++)
            {
                for (int j = i + initialStep; j < points.Count; j++)
                {
                    if (points[i].ApproximatelyAnd(points[j], settings.loopingDistanceDetection))
                    {
                        UnityEngine.Debug.Log($"IsLooping at point[{i}] & point [{j}]\n" +
                                              $"{points[i]} & {points[j]}");
                        
                        if (DrawLine2DDebugHelper) DrawLine2DDebugHelper.ShowPoint(Vector2.Lerp(points[i], points[j], 0.5f), 2f);
                        
                        return true;
                    }
                }
            }
            return false;
        }
    }
}