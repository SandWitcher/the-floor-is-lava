using DrawLine2D.Debug;
using DrawLine2D.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI.Extensions;

namespace DrawLine2D
{
	[RequireComponent(typeof(LineRenderer), typeof(EdgeCollider2D))]
	public class DrawLine2DBehaviour : MonoBehaviour
	{
		#region Editor

		[Header("Scene Objects")]
		
		[SerializeField]
		private Camera drawCamera;

		// [SerializeField] 
		// private Transform ballTransform;

		[SerializeField]
		private GameObject drawLine2DBezierGameObject;

		[SerializeField] 
		private GameObject cursorGameObject;
		
		[Header("Scriptable Objects")]
		
		[SerializeField]
		private DrawLine2DSettings drawLine2DSettings;

		[SerializeField]
		private DrawLine2DData drawLine2DData;

		#endregion
	
	
		private IDrawLine2DInput _drawLine2DInput;
		public IDrawLine2DInput DrawLine2DInput => _drawLine2DInput;

		
		private DrawLine2DWorld _drawLine2DWorld;
		// private DrawLine2DCleaner _drawLine2DCleaner;
		private DrawLine2DPreview _drawLine2DPreview;
		private DrawLine2DBezier _drawLine2DBezier;
		private DrawLine2DEffect _drawLine2DEffect;
		
		protected virtual void Awake ()
		{
			_drawLine2DInput = new StandaloneInput(drawCamera);

			_drawLine2DWorld = new DrawLine2DWorld(_drawLine2DInput,
				drawLine2DSettings,
				drawLine2DData,
				GetComponent<LineRenderer>(),
				GetComponent<EdgeCollider2D>()) {DrawLine2DDebugHelper = FindObjectOfType<DrawLine2DDebugHelper>()};

			_drawLine2DPreview = new DrawLine2DPreview(_drawLine2DInput, 
				drawLine2DSettings,
				drawLine2DData);
			
			// _drawLine2DCleaner = new DrawLine2DCleaner(ballTransform, GetComponent<EdgeCollider2D>());
			
			_drawLine2DBezier = new DrawLine2DBezier(_drawLine2DInput, 
				drawLine2DSettings,
				drawLine2DData,
				drawLine2DBezierGameObject.GetComponent<UILineRenderer>());
			
			_drawLine2DEffect = new DrawLine2DEffect(_drawLine2DInput, 
				drawLine2DSettings,
				drawLine2DData,
				cursorGameObject
			);
		}

		private void Update ()
		{
			_drawLine2DInput.ReadInput();
			_drawLine2DEffect.Tick();
			_drawLine2DPreview.Tick();
			_drawLine2DBezier.Tick();
			_drawLine2DWorld.Tick();
			
			_drawLine2DInput.LateTick();
			_drawLine2DPreview.LateTick();
			_drawLine2DBezier.LateTick();
		}

		// private void LateUpdate()
		// {
		// 	_drawLine2DInput.LateTick();
		// 	_drawLine2DPreview.LateTick();
		// 	_drawLine2DBezier.LateTick();
		// }

		private void OnDestroy()
		{
			drawLine2DData.Reset();
		}

		// #region Collision callbacks
		//
		// private void OnCollisionEnter2D(Collision2D other) => _drawLine2DCleaner.OnCollisionEnter2D(other);
		//
		// private void OnCollisionStay2D(Collision2D other) => _drawLine2DCleaner.OnCollisionStay2D(other);
		//
		// private void OnCollisionExit2D(Collision2D other) => _drawLine2DCleaner.OnCollisionExit2D(other);
		//
		// #endregion
		//
		//
		// #region Trigger callbacks
		//
		// private void OnTriggerEnter2D(Collider2D other) => _drawLine2DCleaner.OnTriggerEnter2D(other);
		//
		// private void OnTriggerStay2D(Collider2D other) => _drawLine2DCleaner.OnTriggerStay2D(other);
		//
		// private void OnTriggerExit2D(Collider2D other) => _drawLine2DCleaner.OnTriggerExit2D(other);
		//
		// #endregion
		
	}
}