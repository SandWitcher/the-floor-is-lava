﻿#if UNITY_EDITOR
using System;
using UnityEditor;
using Object = UnityEngine.Object;

public static class EditorExtensions
{
	public static Object GetAsset(this Type objectType)
	{
		var guids = AssetDatabase.FindAssets($"t:{objectType.Name}");
		if (guids.Length <= 0) throw new Exception($"{objectType.Name} not found");
		string assetPath = AssetDatabase.GUIDToAssetPath (guids[0]);
		return AssetDatabase.LoadAssetAtPath(assetPath, objectType);
	}
}
#endif