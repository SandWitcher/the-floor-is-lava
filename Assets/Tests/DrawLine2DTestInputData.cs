﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DrawLine2D.Tests;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
	public class DrawLine2DTestInputData : ADrawLine2DTest
	{
		[OneTimeSetUp]
		public override void OneTimeSetup()
		{
			base.OneTimeSetup();
		}

		[SetUp]
		public void SetUp()
		{
			var properties = input.GetType().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public);

			foreach (var property in properties)
			{
				if (property.IsOfType<Vector2>())
					property.SetValue(input, default);
			}
			
			(input as MockDrawLine2DInput)!.ResetMockedProperties();
			
			data.Reset();
		}

		#region Input Reset

		[UnityTest]
		public IEnumerator InputReset_Resets_Preview_Data()
		{
			// Arrange
			data.previewPoints.AddRange(Enumerable.Repeat(new Vector2(Random.value * 10, Random.value * 10), 10));
			
			// Act
			yield return WaitAFrame();
			input.Reset.Returns(true);
			yield return WaitAFrame();
			input.Reset.Returns(false);
			yield return WaitAFrame();

			// Assert
			Assert.IsTrue(data.previewPoints.Count <= 1);
		}
		
		[UnityTest]
		public IEnumerator InputReset_Sets_PreviewBezier_FirstElem_Data()
		{
			// Arrange
			data.previewPoints.AddRange(Enumerable.Repeat(new Vector2(Random.value * 10, Random.value * 10), 10));
			
			// Act
			yield return WaitAFrame();
			input.Reset.Returns(true);
			yield return WaitAFrame();
			input.Reset.Returns(false);
			yield return WaitAFrame();
			
			// Assert
			Assert.IsTrue(data.bezierPoints[0].ApproximatelyAnd(data.previewPoints[0]));
		}

		#endregion

		#region Input IsPreviewing
		
		[UnityTest]
		public IEnumerator InputIsPreviewing_Updates_Preview_Data()
		{
			// Arrange
			
			// Act
			input.IsPreviewing.Returns(true);
			
			for (int i = 0; i < 6; i++)
			{
				yield return WaitAFrame();
			}

			// Assert
			Assert.IsTrue(data.previewPoints.Count >= 5 && data.previewPoints.All(p => p != Vector2.zero));
		}

		#endregion


		#region Input Cancel

		[UnityTest]
		public IEnumerator InputCancel_Resets_Preview_And_PreviewBezier_Data_And_Stops_Previewing()
		{
			// Arrange
			
			// Act
			input.IsPreviewing.Returns(true);
			
			for (int i = 0; i < 5; i++)
			{
				yield return WaitAFrame();
			}

			input.CancelPreview.Returns(true);

			yield return WaitAFrame();

			input.CancelPreview.Returns(false);
			
			for (int i = 0; i < 5; i++)
			{
				yield return WaitAFrame();
			}
			
			// Assert
			Assert.IsTrue(data.previewPoints.Count <= 0 && data.bezierPoints.All(p => p == Vector2.zero));
		}		

		#endregion


		#region Input HasFinishedPreviewing

		[UnityTest]
		public IEnumerator InputHasFinishedPreviewing_Updates_World_And_WorldBezier_Data()
		{
			// Arrange
			var initialPoints = new List<Vector2>(data.worldPoints);
			var initialBezierPoints = new List<Vector2>(data.worldBezierPoints);
			
			// Act
			input.IsPreviewing.Returns(true);
			
			for (int i = 0; i < 5; i++)
			{
				yield return WaitAFrame();
			}

			input.IsPreviewing.Returns(false);
			input.HasFinishedPreview.Returns(true);
			
			yield return WaitAFrame();

			input.HasFinishedPreview.Returns(false);
			
			yield return WaitAFrame();
			
			// Assert
			data.worldPoints.ForEach(worldPoint => Assert.IsTrue(initialPoints.All(p => p != worldPoint)));
			data.worldBezierPoints.ForEach(bezierWorldPoint => Assert.IsTrue(initialBezierPoints.All(p => p != bezierWorldPoint)));
		}
		
		[UnityTest]
		public IEnumerator InputHasFinishedPreviewing_Resets_Preview_And_PreviewBezier_Data()
		{
			// Arrange
			
			// Act
			input.IsPreviewing.Returns(true);
			
			for (int i = 0; i < 5; i++)
			{
				yield return WaitAFrame();
			}

			input.IsPreviewing.Returns(false);
			
			input.HasFinishedPreview.Returns(true);
			
			yield return WaitAFrame();

			input.HasFinishedPreview.Returns(false);
			
			yield return WaitAFrame();
			
			// Assert
			Assert.IsTrue(data.bezierPoints.All(p => p == Vector2.zero));
			Assert.IsTrue(data.previewPoints.Count <= 0);
		}

		#endregion


		#region Input IsOverUI

		#endregion
	}
}