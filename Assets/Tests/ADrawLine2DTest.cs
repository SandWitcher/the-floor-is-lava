﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using DrawLine2D;
using DrawLine2D.ScriptableObjects;
using DrawLine2D.Tests;
using DrawLine2D.Tests.PlayMode;
using JetBrains.Annotations;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.UI.Extensions;

namespace Tests
{
	public abstract class ADrawLine2DTest
	{
		#region Behaviour & Classes

		protected DrawLine2DBehaviour behaviour;
		protected DrawLine2DBezier bezier;
		protected DrawLine2DPreview preview;
		protected DrawLine2DWorld world;
		protected DrawLine2DEffect effect;		

		#endregion
		
		#region Scriptable Objects

		protected DrawLine2DData data;
		protected DrawLine2DSettings settings;
		protected DrawLine2DPlayModeTestSO so;

		#endregion
		
		#region Inputs

		protected IDrawLine2DInput input;
		protected Camera drawCamera;
		
		#endregion

		#region Other

		protected Canvas canvas;
		
		#endregion

		[OneTimeSetUp]
		public virtual void OneTimeSetup()
		{
			data = typeof(DrawLine2DData).GetAsset() as DrawLine2DData;
			settings = typeof(DrawLine2DSettings).GetAsset() as DrawLine2DSettings;
			so = typeof(DrawLine2DPlayModeTestSO).GetAsset() as DrawLine2DPlayModeTestSO;

			// SceneManager.LoadScene("Scenes/SampleScene");
			
			// drawCamera = Camera.main;
			
			drawCamera = InstantiateObject<Camera>("Camera");
			drawCamera.orthographic = true;
			drawCamera.tag = "MainCamera";
			
			input = Substitute.For<MockDrawLine2DInput>(drawCamera);

			behaviour = InstantiateObject<MockDrawLine2DBehaviour>("Behaviour");

			bezier = new DrawLine2DBezier(input, settings, data, InstantiateObject<UILineRenderer>("Bezier"));
			preview = new DrawLine2DPreview(input, settings, data);
			world = new DrawLine2DWorld(input, settings, data, behaviour.GetComponent<LineRenderer>(), behaviour.GetComponent<EdgeCollider2D>());
			effect = new DrawLine2DEffect(input, settings, data, InstantiateUIObject<ParticleSystem>(ref canvas).gameObject);
			
			SetGameObjectFields(behaviour, typeof(DrawLine2DBehaviour), bezier, preview, world, effect, settings, data);
			 
			SetGameObjectSubstitute(behaviour, typeof(DrawLine2DBehaviour), input, typeof(IDrawLine2DInput));
		}

		private static void SetGameObjectFields(object instance, Type t, params object[] inputFields)
		{
			var fields = t.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);

			foreach (var inputField in inputFields)
			{
				
				var objectField = fields.FirstOrDefault(field => field.FieldType == inputField.GetType());
				
				if (objectField != null)
				{
					objectField.SetValue(instance, inputField);	
				}
			}
		}

		private static void SetGameObjectSubstitute(object instance, Type instanceType, object substitute, Type substituteType)
		{
			var fields = instanceType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);

			var objectField = fields.FirstOrDefault(field => field.FieldType == substituteType);
			
			if (objectField != null)
			{
				objectField.SetValue(instance, substitute);	
			}
		}
		
		
		private static T InstantiateObject<T>(string name = null ) where T : Component
		{
			return new GameObject(name).AddComponent<T>();
		}

		private static T InstantiateUIObject<T>([CanBeNull] ref Canvas c, string name = null ) where T : Component
		{
			if (c == null) c = new GameObject("Canvas").AddComponent<Canvas>();
			
			var obj = new GameObject(name);
			obj.transform.SetParent(c.transform);
			
			 var rectTransform  = obj.AddComponent<RectTransform>();
			rectTransform.position = Vector3.zero;
			rectTransform.localScale = Vector3.one;
			
			obj.AddComponent<CanvasRenderer>();
			return obj.AddComponent<T>();
		}
		
		protected static IEnumerator WaitAFrame()
		{
			yield return Application.isBatchMode ? null : new WaitForEndOfFrame();
		}
	}
}



















