using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DrawLine2D;
using DrawLine2D.Tests;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class BallDrawInteractionTests
    {
        private GameObject _ball;
        private GameObject _drawLine2D;
        private GameObject _camera;

        [SetUp]
        public void Setup()
        {

            _camera = Object.Instantiate(new GameObject());
            _camera.AddComponent<Camera>();
            _camera.GetComponent<Camera>().orthographic = true;
            _camera.tag = "MainCamera";
            
            _ball = Object.Instantiate(new GameObject());
            _ball.AddComponent<CircleCollider2D>();
            _ball.GetComponent<CircleCollider2D>().radius = 0.5f;
            _ball.AddComponent<Rigidbody2D>();
            
            _drawLine2D = Object.Instantiate(new GameObject());
            _drawLine2D.AddComponent<DrawLine2DBehaviour>(); // adds EdgeCollider2D & LineRenderer
            
        }

        // [UnityTest]
        // public IEnumerator BallGoesThroughLooping()
        // {
        //
        //     _ball.transform.position = drawLineSO.startPosition;
        //     _ball.GetComponent<Rigidbody2D>().velocity = drawLineSO.velocity;
        //   //  _drawLine2D.GetComponent<DrawLine2DBehaviour>().TestDrawLine(drawLineSO.drawPoints);
        //     
        //     yield return new WaitForSeconds(3);
        //     
        //     Assert.True(_ball.transform.position.x > drawLineSO.endPosition.x);
        // }
        
    }
}


