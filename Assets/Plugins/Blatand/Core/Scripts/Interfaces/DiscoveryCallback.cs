﻿using Blatand.Utils;
using UnityEngine;

namespace Blatand.Core
{
    /// <summary>
    /// This abstract class is used to implement BluetoothGatt callbacks. 
    /// </summary>
    public abstract class DiscoveryCallback : AndroidJavaProxy
    {
       
        public DiscoveryCallback() : base("com.blatand.DiscoveryCallback")
        {
        }

        public virtual void OnDiscoveryStarted()
        {
        }

        public virtual void OnDiscoveryEnded()
        {
        }

        public virtual void OnDeviceDiscovered(/* BluetoothDevice */ AndroidJavaObject result)
        {
        }
    }
}

