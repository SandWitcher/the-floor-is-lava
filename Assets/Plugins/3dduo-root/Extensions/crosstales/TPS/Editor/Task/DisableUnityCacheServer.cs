﻿using UnityEngine;
using UnityEditor;
using System.Collections;
namespace Crosstales.TPS.Task
{
    /// <summary>Disables the Unity cache server.</summary>
    [InitializeOnLoad]
    public static class DisableUnityCacheServer {

        #region Constructor

        static DisableUnityCacheServer()
        {
            if (EditorPrefs.GetInt("CacheServerMode") != 2)
            {
                EditorPrefs.SetInt("CacheServerMode", 2);

                Debug.LogWarning("+++ 'Unity Cache Server' has been disabled for TPS! +++");
            }
        }
        
        #endregion
    }
}