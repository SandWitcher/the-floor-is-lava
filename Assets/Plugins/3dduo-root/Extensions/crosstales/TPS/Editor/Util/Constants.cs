﻿using UnityEngine;

namespace Crosstales.TPS.Util
{
    /// <summary>Collected constants of very general utility for the asset.</summary>
    public abstract class Constants : Common.Util.BaseConstants
    {

        #region Constant variables

        /// <summary>Is PRO-version?</summary>
        public static readonly bool isPro = true;

        /// <summary>Name of the asset.</summary>
        public const string ASSET_NAME = "TPS PRO";
        //public const string ASSET_NAME = "TPS";

        /// <summary>Version of the asset.</summary>
        public const string ASSET_VERSION = "1.8.2";

        /// <summary>Build number of the asset.</summary>
        public const int ASSET_BUILD = 182;

        /// <summary>Create date of the asset (YYYY, MM, DD).</summary>
        public static readonly System.DateTime ASSET_CREATED = new System.DateTime(2016, 9, 22);

        /// <summary>Change date of the asset (YYYY, MM, DD).</summary>
        public static readonly System.DateTime ASSET_CHANGED = new System.DateTime(2018, 3, 26);

        /// <summary>URL of the PRO asset in UAS.</summary>
        public const string ASSET_PRO_URL = "https://www.assetstore.unity3d.com/#!/content/60040?aid=1011lNGT&pubref=" + ASSET_NAME;

        /// <summary>URL for update-checks of the asset</summary>
        public const string ASSET_UPDATE_CHECK_URL = "https://www.crosstales.com/media/assets/tps_versions.txt";
        //public const string ASSET_UPDATE_CHECK_URL = "https://www.crosstales.com/media/assets/test/tps_versions_test.txt";

        /// <summary>Contact to the owner of the asset.</summary>
        public const string ASSET_CONTACT = "tps@crosstales.com";

        /// <summary>URL of the asset manual.</summary>
        public const string ASSET_MANUAL_URL = "https://www.crosstales.com/media/data/assets/tps/TPS-doc.pdf";

        /// <summary>URL of the asset API.</summary>
        public const string ASSET_API_URL = "https://goo.gl/NDTja0";
        //public const string ASSET_API_URL = "http://www.crosstales.com/en/assets/tps/api/";

        /// <summary>URL of the asset forum.</summary>
        public const string ASSET_FORUM_URL = "https://goo.gl/d7SjL2";
        //public const string ASSET_FORUM_URL = "https://forum.unity3d.com/threads/turbo-platform-switch.434860/";

        /// <summary>URL of the asset in crosstales.</summary>
        public const string ASSET_WEB_URL = "https://www.crosstales.com/en/portfolio/tps/";

        /// <summary>URL of the promotion video of the asset (Youtube).</summary>
        public const string ASSET_VIDEO_PROMO = "https://youtu.be/rb1cqypznEg?list=PLgtonIOr6Tb41XTMeeZ836tjHlKgOO84S";

        /// <summary>URL of the tutorial video of the asset (Youtube).</summary>
        public const string ASSET_VIDEO_TUTORIAL = "https://youtu.be/J2zh0EjmrjQ?list=PLgtonIOr6Tb41XTMeeZ836tjHlKgOO84S";


        // Keys for the configuration of the asset
        private const string KEY_PREFIX = "TPS_CFG_";
        public const string KEY_CUSTOM_PATH_CACHE = KEY_PREFIX + "CUSTOM_PATH_CACHE";
        public const string KEY_PATH_CACHE = KEY_PREFIX + "PATH_CACHE";
        public const string KEY_VCS = KEY_PREFIX + "VCS";
        public const string KEY_BATCHMODE = KEY_PREFIX + "BATCHMODE";
        public const string KEY_QUIT = KEY_PREFIX + "QUIT";
        public const string KEY_NO_GRAPHICS = KEY_PREFIX + "NO_GRAPHICS";
        public const string KEY_EXECUTE_METHOD = KEY_PREFIX + "EXECUTE_METHOD";
        public const string KEY_COPY_SETTINGS = KEY_PREFIX + "COPY_SETTINGS";
        public const string KEY_COPY_ASSETS = KEY_PREFIX + "COPY_ASSETS";
        public const string KEY_CONFIRM_SWITCH = KEY_PREFIX + "CONFIRM_SWITCH";
        public const string KEY_DEBUG = KEY_PREFIX + "DEBUG";
        public const string KEY_UPDATE_CHECK = KEY_PREFIX + "UPDATE_CHECK";
        public const string KEY_UPDATE_OPEN_UAS = KEY_PREFIX + "UPDATE_OPEN_UAS";
        public const string KEY_REMINDER_CHECK = KEY_PREFIX + "REMINDER_CHECK";
        public const string KEY_TELEMETRY = KEY_PREFIX + "TELEMETRY";

        public const string KEY_PLATFORM_WINDOWS = KEY_PREFIX + "PLATFORM_WINDOWS";
        public const string KEY_PLATFORM_MAC = KEY_PREFIX + "PLATFORM_MAC";
        public const string KEY_PLATFORM_LINUX = KEY_PREFIX + "PLATFORM_LINUX";
        public const string KEY_PLATFORM_ANDROID = KEY_PREFIX + "PLATFORM_ANDROID";
        public const string KEY_PLATFORM_IOS = KEY_PREFIX + "PLATFORM_IOS";
        public const string KEY_PLATFORM_WSA = KEY_PREFIX + "PLATFORM_WSA";
        public const string KEY_PLATFORM_WEBPLAYER = KEY_PREFIX + "PLATFORM_WEBPLAYER";
        public const string KEY_PLATFORM_WEBGL = KEY_PREFIX + "PLATFORM_WEBGL";
        public const string KEY_PLATFORM_TVOS = KEY_PREFIX + "PLATFORM_TVOS";
        public const string KEY_PLATFORM_TIZEN = KEY_PREFIX + "PLATFORM_TIZEN";
        public const string KEY_PLATFORM_SAMSUNGTV = KEY_PREFIX + "PLATFORM_SAMSUNGTV";
        public const string KEY_PLATFORM_PS3 = KEY_PREFIX + "PLATFORM_PS3";
        public const string KEY_PLATFORM_PS4 = KEY_PREFIX + "PLATFORM_PS4";
        public const string KEY_PLATFORM_PSP2 = KEY_PREFIX + "PLATFORM_PSP2";
        public const string KEY_PLATFORM_XBOX360 = KEY_PREFIX + "PLATFORM_XBOX360";
        public const string KEY_PLATFORM_XBOXONE = KEY_PREFIX + "PLATFORM_XBOXONE";
        public const string KEY_PLATFORM_WIIU = KEY_PREFIX + "PLATFORM_WIIU";
        public const string KEY_PLATFORM_3DS = KEY_PREFIX + "PLATFORM_3DS";
        public const string KEY_PLATFORM_SWITCH = KEY_PREFIX + "PLATFORM_SWITCH";

        public const string KEY_ARCH_WINDOWS = KEY_PREFIX + "ARCH_WINDOWS";
        public const string KEY_ARCH_MAC = KEY_PREFIX + "ARCH_MAC";
        public const string KEY_ARCH_LINUX = KEY_PREFIX + "ARCH_LINUX";

        public const string KEY_TEX_ANDROID = KEY_PREFIX + "TEX_ANDROID";

        public const string KEY_SHOW_COLUMN_PLATFORM = KEY_PREFIX + "SHOW_COLUMN_PLATFORM";
        public const string KEY_SHOW_COLUMN_ARCHITECTURE = KEY_PREFIX + "SHOW_COLUMN_ARCHITECTURE";
        public const string KEY_SHOW_COLUMN_TEXTURE = KEY_PREFIX + "SHOW_COLUMN_TEXTURE";
        public const string KEY_SHOW_COLUMN_CACHE = KEY_PREFIX + "SHOW_COLUMN_CACHE";

        public const string KEY_UPDATE_DATE = KEY_PREFIX + "UPDATE_DATE";

        public const string KEY_REMINDER_DATE = KEY_PREFIX + "REMINDER_DATE";
        public const string KEY_REMINDER_COUNT = KEY_PREFIX + "REMINDER_COUNT";

        public const string KEY_LAUNCH = KEY_PREFIX + "LAUNCH";

        public const string KEY_TELEMETRY_DATE = KEY_PREFIX + "TELEMETRY_DATE";

        public const string CACHE_DIRNAME = "TPS_cache";

        /// <summary>Application path.</summary>
        public static readonly string PATH = Helper.ValidatePath(Application.dataPath.Substring(0, Application.dataPath.LastIndexOf('/') + 1));

        // Default values
        public const string DEFAULT_ASSET_PATH = "/Plugins/crosstales/TPS/";
        public static readonly string DEFAULT_PATH_CACHE = Helper.ValidatePath(PATH + CACHE_DIRNAME);
        public const bool DEFAULT_CUSTOM_PATH_CACHE = false;
        public const int DEFAULT_VCS = 1; //git
        public const bool DEFAULT_BATCHMODE = false;
        public const bool DEFAULT_QUIT = true;
        public const bool DEFAULT_NO_GRAPHICS = false;
        public const bool DEFAULT_COPY_SETTINGS = false;
        public const bool DEFAULT_COPY_ASSETS = false;
        public const bool DEFAULT_CONFIRM_SWITCH = true;
        public const bool DEFAULT_UPDATE_CHECK = true;
        public const bool DEFAULT_REMINDER_CHECK = true;
        public const bool DEFAULT_TELEMETRY = true;

        public const bool DEFAULT_PLATFORM_WINDOWS = true;
        public const bool DEFAULT_PLATFORM_MAC = true;
        public const bool DEFAULT_PLATFORM_LINUX = true;
        public const bool DEFAULT_PLATFORM_ANDROID = true;
        public const bool DEFAULT_PLATFORM_IOS = true;
        public const bool DEFAULT_PLATFORM_WSA = true;
        public const bool DEFAULT_PLATFORM_WEBPLAYER = false;
        public const bool DEFAULT_PLATFORM_WEBGL = true;
        public const bool DEFAULT_PLATFORM_TVOS = false;
        public const bool DEFAULT_PLATFORM_TIZEN = false;
        public const bool DEFAULT_PLATFORM_SAMSUNGTV = false;
        public const bool DEFAULT_PLATFORM_PS3 = false;
        public const bool DEFAULT_PLATFORM_PS4 = false;
        public const bool DEFAULT_PLATFORM_PSP2 = false;
        public const bool DEFAULT_PLATFORM_XBOX360 = false;
        public const bool DEFAULT_PLATFORM_XBOXONE = false;
        public const bool DEFAULT_PLATFORM_WIIU = false;
        public const bool DEFAULT_PLATFORM_3DS = false;
        public const bool DEFAULT_PLATFORM_SWITCH = false;

#if UNITY_2017 || UNITY_2018
        public const int DEFAULT_ARCH_WINDOWS = 1;
        public const int DEFAULT_ARCH_MAC = 1;
        public const int DEFAULT_ARCH_LINUX = 1;
#else
        public const int DEFAULT_ARCH_WINDOWS = 0;
        public const int DEFAULT_ARCH_MAC = 0;
        public const int DEFAULT_ARCH_LINUX = 0;
#endif
        public const int DEFAULT_TEX_ANDROID = 0;
        public const bool DEFAULT_SHOW_COLUMN_PLATFORM = true;
        public const bool DEFAULT_SHOW_COLUMN_PLATFORM_LOGO = false;
        public const bool DEFAULT_SHOW_COLUMN_ARCHITECTURE = true;
        public const bool DEFAULT_SHOW_COLUMN_TEXTURE = false;
        public const bool DEFAULT_SHOW_COLUMN_CACHE = true;

        #endregion


        #region Properties

        /// <summary>Returns the URL of the asset in UAS.</summary>
        /// <returns>The URL of the asset in UAS.</returns>
        public static string ASSET_URL
        {
            get
            {

                if (isPro)
                {
                    return ASSET_PRO_URL;
                }
                else
                {
                    return "https://www.assetstore.unity3d.com/#!/content/98900?aid=1011lNGT&pubref=" + ASSET_NAME;
                }
            }
        }

        /// <summary>Returns the UID of the asset.</summary>
        /// <returns>The UID of the asset.</returns>
        public static System.Guid ASSET_UID
        {
            get
            {
                if (isPro)
                {
                    return new System.Guid("2d03d693-219a-4fa4-a9b0-83e5a59ebe01");
                }
                else
                {
                    return new System.Guid("a511a1ad-577e-4d51-92f4-112120762440");
                }
            }
        }

        #endregion


        #region Changable variables

        // Technical settings
        /// <summary>Kill processes after 5000 milliseconds.</summary>
        public static int KILL_TIME = 5000;

        #endregion

    }
}
// © 2016-2018 crosstales LLC (https://www.crosstales.com)