﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIChooseRectPositionByIndex : MonoBehaviour {

    private int currentIndex = 0;

    public float TweenPanelDuration = .25f;

    public List<RectTransform> Buttons = new List<RectTransform>();

    public List<Vector2> TopPositions = new List<Vector2>();
    public List<Vector2> BotPositions = new List<Vector2>();



    private void Awake() {
        currentIndex = 0;
        for(int i = 1; i < Buttons.Count; i++) {
            //take top positions
            if(i <= currentIndex) {
                Buttons[i].anchoredPosition = TopPositions[i-1];
            } else {
                Buttons[i].anchoredPosition = BotPositions[i-1];
            }
        }

        
    }

    public void OnButtonClick(int _index) {

        if(_index == currentIndex) {
            return;
        }
        currentIndex = _index;

        for(int i = 1; i < Buttons.Count; i++){
            //take top positions
            if(i <= currentIndex) {
                //Buttons[i].anchoredPosition = TopPositions[i-1];
                Buttons[i].DOAnchorPos(TopPositions[i - 1], TweenPanelDuration);
            } else {
                //Buttons[i].anchoredPosition = BotPositions[i-1];
                Buttons[i].DOAnchorPos(BotPositions[i - 1], TweenPanelDuration);
            }
        }
    }
	
}
