﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UITab : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {

    public Sprite NormalSprite;

    public Sprite SelectedSprite;

    [SerializeField]
    Image mImage = default;

    private bool isSelected = false;
    private UITabSystem tabSystem;

    public void Init(UITabSystem _tabSystem) {
        tabSystem = _tabSystem;
    }


    public void Select(bool _isSelected) {
        if(mImage == null) {
            return;
        }
        isSelected = _isSelected;
        mImage.sprite = _isSelected ? SelectedSprite : NormalSprite;
    }

    public void OnPointerEnter(PointerEventData eventData) {
        if(mImage == null) {
            return;
        }

        if(!isSelected) {
            mImage.sprite = SelectedSprite;
        }
    }

    public void OnPointerExit(PointerEventData eventData) {
        if(mImage == null) {
            return;
        }

        if(!isSelected) {
            mImage.sprite = NormalSprite;
        }
    }

    public void OnPointerClick(PointerEventData eventData) {
        if(!isSelected && tabSystem != null) {
            tabSystem.SelectedTab = this;
        }
    }
}
