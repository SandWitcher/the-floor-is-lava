using System;
using DrawLine2D;
using DrawLine2D.ScriptableObjects;
using DrawLine2D.Tests;
using DrawLine2D.Tests.EditMode;
using NSubstitute;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Editor.EditModeTests
{
    public class DrawLine2DLoopingTest
    {
        private DrawLine2DSettings _drawLine2DSettings;
        private DrawLine2DEditModeTestSO _drawLine2DEditTestModeSO;
        private ADrawLine2DBase _drawLine2D;
        
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _drawLine2DSettings = typeof(DrawLine2DSettings).GetAsset() as DrawLine2DSettings;
            _drawLine2DEditTestModeSO = typeof(DrawLine2DEditModeTestSO).GetAsset() as DrawLine2DEditModeTestSO;
            _drawLine2D = Substitute.For<ADrawLine2DBase>(null, _drawLine2DSettings, null, null);
        }
        
        
        // [Test]
        // public void LoopingTest()
        // {
        //     for (var i = 0; i < _drawLine2DEditTestModeSO.loopings.Count; i++)
        //     {
        //         var looping = _drawLine2DEditTestModeSO.loopings[i];
        //         Debug.Log($"loopings[{i}]");
        //         Assert.IsTrue(_drawLine2D.CheckIsLooping(looping.points));
        //     }
        // }
        //
        // [Test]
        // public void NotLoopingTest()
        // {
        //     for (var i = 0; i < _drawLine2DEditTestModeSO.notLoopings.Count; i++)
        //     {
        //         var notLooping = _drawLine2DEditTestModeSO.notLoopings[i];
        //         Debug.Log($"notLoopings[{i}]");
        //         Assert.IsFalse(_drawLine2D.CheckIsLooping(notLooping.points));
        //     }
        // }
    }
}
